<?php

/**
 * @file
 * Displays a block containing a node's information.
 */


/**
 * Implementation of hook_menu()
 */
function about_this_node_menu() {
  $items = array();
  
  $items['admin/content/aboutthisnode'] = array(
    'title' => t('About this node'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('about_this_node_admin_settings'),
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer about this node'),
  );
  
  return $items;
}


/**
 * Implementation of hook_perm()
 */
function about_this_node_perm() {
  return array('administer about this node', 'view about this node block');
}


/**
 * Implementation of hook_theme()
 */
function about_this_node_theme() {
  return array(
    'about_this_node_node' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}


/**
 * Implementation of hook_admin_settings()
 */
function about_this_node_admin_settings() {
  $form['about_this_node_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('View information about these node types'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('about_this_node_nodetypes', array('story')),
    '#description' => t('The about this node block will appear when viewing these node types.'),
  );

  return system_settings_form($form);
}


/**
 * Implementation of hook_block()
 */
function about_this_node_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {

    // Displays block name in admin/build/blocks
    case 'list':
      $blocks[0]['info'] = t('About This Node');
      return $blocks;

    // Generates block output
    case 'view':
      if (user_access('view about this node block')) {
        $block['subject'] = t('About This Node');
        $block['content'] = about_this_node_block_contents();
        return $block;
      }
  }
}


/**
 * Prepares the block and routes it to the appropriate theme function
 */
function about_this_node_block_contents() {
  $output = '';
  $id = arg(1);
  $node_info = array();

  if (is_numeric($id)) {
    $pagetype = arg(0);

    switch ($pagetype) {

      // This version of about_this_node only displays a block for node page types (i.e., not user pages, etc.)
      case 'node':
        $node = node_load($id);
        $allowed_nodetypes = variable_get('about_this_node_nodetypes', array('story'));

        // Conditional will return 0 (FALSE) if it isn't an allowed node type
        if ($allowed_nodetypes[$node->type]) {
          $output .= theme('about_this_node_node', about_this_node_get_info($node), $node);
        }
        break;

    }
  }

  return $output;
}


/**
 * Gather information about the node and return it as a keyed array
 */
function about_this_node_get_info($node) {
  $node_info = array();

  // Node ID
  $node_info['node_id'] = array(
    'label' => 'Node ID (NID):',
    'value' => $node->nid,
  );

  // Node type
  $node_info['node_type'] = array(
    'label' => 'Node type:',
    'value' => node_get_types('name', $node),
  );

  // Gather author info
  $author_uid = $node->uid;
  $author = '';
  $author_output = '';
  if ($author_uid == '0') { // Check if author is anonymous
    $author_output = variable_get('anonymous', 'Anonymous');
  }
  else {
    $author = user_load(array('uid' => $author_uid));
    $author_output = l($author->name, 'user/' . $author_uid);
  }

  // Author name/time
  $node_info['created'] = array(
    'label' => 'Created:',
    'value' => array(
      'created_on' => array(
        'label' => 'on',
        'value' => $node->created,
      ),
      'created_by' => array(
        'label' => 'by',
        'value' => $author_output,
      ),
    ),
  );

  // Gather last updated author info
  $update_author_uid = db_result(db_query('SELECT nr.uid FROM {node_revisions} nr INNER JOIN {node} n ON n.vid = nr.vid WHERE n.nid = %d', $node->nid));
  $update_author = '';
  $update_author_output = '';
  if ($update_author_uid == '0') { // Check if author is anonymous
    $update_author_output = variable_get('anonymous', 'Anonymous');
  }
  else {
    $update_author = user_load(array('uid' => $update_author_uid));
    $update_author_output = l($update_author->name, 'user/' . $update_author_uid);
  }

  // Last updated author name/time
  $node_info['updated'] = array(
    'label' => 'Last updated:',
  );

  // Check if node has never been updated
  if ($node->created == $node->changed) {
    $node_info['updated']['value'] = 'Never';
  }
  else {
    $node_info['updated']['value'] = array(
      'updated_on' => array(
        'label' => 'on',
        'value' => $node->changed,
      ),
      'updated_by' => array(
        'label' => 'by',
        'value' => $update_author_output,
      ),
    );
  }

  // Published status
  $node_info['published'] = array(
    'label' => 'Published:',
  );
  if ($node->status == 1) {
    $node_info['published']['value'] = 'Yes';
  }
  else {
    $node_info['published']['value'] = 'No';
  }

  // Promoted to front page status
  $node_info['promoted'] = array(
    'label' => 'Promoted:',
  );
  if ($node->promote == 1) {
    $node_info['promoted']['value'] = 'Yes';
  }
  else {
    $node_info['promoted']['value'] = 'No';
  }

  // Sticky status
  $node_info['stickied'] = array(
    'label' => 'Stickied:',
  );
  if ($node->sticky == 1) {
    $node_info['stickied']['value'] = 'Yes';
  }
  else {
    $node_info['stickied']['value'] = 'No';
  }

  // Commenting status
  $node_info['commenting'] = array(
    'label' => 'Commenting:',
  );
  if (module_exists('comment')) {
    if ($node->comment == 0) {
      $node_info['commenting']['value'] = 'Disabled';
    }
    else if ($node->comment == 1) {
      $node_info['commenting']['value'] = 'Read only';
    }
    else {
      $node_info['commenting']['value'] = 'Read/Write';
    }
  }
  else {
    $node_info['commenting']['value'] = 'Module not enabled';
  }

  return $node_info;
}


/**
 * Prepare keyed array for theming as an item-list
 */
function about_this_node_prepare_info($node_info, $date_format = 'small') {
  $node_info_prepped = array();

  // Format some dates
  if (isset($node_info['created']['value']['created_on']['value'])) {
    $node_info['created']['value']['created_on']['value'] = format_date($node_info['created']['value']['created_on']['value'], $date_format);
  }
  if (isset($node_info['updated']['value']['updated_on']['value'])) {
    $node_info['updated']['value']['updated_on']['value'] = format_date($node_info['updated']['value']['updated_on']['value'], $date_format);
  }

  foreach ($node_info as $key => $item) {
    $node_info_prepped[$key] = array('data' => '<span class="aboutthisnode-label">' . $item['label'] . '</span> ');

    // If this item has children
    if (is_array($item['value'])) {
      $node_info_prepped[$key]['children'] = about_this_node_prepare_info($item['value']);
    }
    else {
      $node_info_prepped[$key]['data'] .= $item['value'];
    }
  }

  return $node_info_prepped;
}



/**
 * Theme function that build the block's output for nodes
 */
function theme_about_this_node_node($node_info, $node) {
  return theme('item_list', about_this_node_prepare_info($node_info, 'small'));
}
